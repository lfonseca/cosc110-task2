# Programming Task 2

This program takes a list of codons (each represented as a string of three G, C, A, 
or T characters) from the COVID-19 virus S protein and modifies it to better match the 
codons in the extremely efficient BioNTech/Pfizer SARS-CoV-2 Vaccine S protein.

## Getting Started

### Prerequisites

```
Python 3
TerminalColor
```

### Installing

Extract the file pt2.tgz and enter it into the pt2 directory

```
$ tar -zxf pt2.tgz
$ cd pt2
```

### Running

Make sure that pt2 directory contains:

```
|____LICENSE.md
|____virus.txt
|____skeleton.py
|____vaccine.txt
|____readme_resources
|   |____ss_default_run.png
|   |____ss_average_run.png
|   |____logs_dir.png
|   |____log_example.png
|   |____ss_debug_run.png
|   |____ss_debug_result.png
|____codon_optimisation.py
|____codon_properties.py
|____readme.md
|____codon_rules.py
|____TerminalColor.py
|____codon_tools.py
|____codon_debug_tools.py
|____codon-aminoacid.csv
```

Using the python Command

```
$ python3 codon_optimisation.py
```

Using the Script Filename

```
$ chmod +x codon_optimisation.py
$ ./codon_optimisation.py
```

![Alt text](readme_resources/ss_default_run.png?raw=true "Example Task 02 - Default Run")

### Verbose Mode -v
You can use a verbose mode to see on the screen the optimisation process. It also creates a log
file at logs/log_xxx.txt

```
$ python3 codon_optimisation.py -v
```

![Alt text](readme_resources/ss_debug_run.png?raw=true "Example Task 02 - Debug Run")

#### Result:
![Alt text](readme_resources/ss_debug_result.png?raw=true "Example Task 02 - Result Debug")

### Logs
#### Directory:
![Alt text](readme_resources/logs_dir.png?raw=true "Example Task 02 - Logs Dir")

#### Log Example:
![Alt text](readme_resources/log_example.png?raw=true "Example Task 02 - Log Example")

### Show Average -a
You can check the 20 runs average using the -a parameter. 

```
$ python3 codon_optimisation.py -a
```

![Alt text](readme_resources/ss_average_run.png?raw=true "Example Task 02 - Average Run")


## Files
A brief explanation about the files

### virus.txt
Unprocessed RNA of the virus S protein.

### vaccine.txt
Unprocessed RNA of the vaccine S protein.

### codon-aminoacid.csv
Mapping codons to the amino acid they produce.

### skeleton.py
Help guide script to start the project contains basic functions to read the codons and calculate the optimisation.

### codon_optimisation.py
Main script that runs optimisation rules and generates a new set of codons to be compared to the vaccine.

### codon_properties.py
Set of codon properties gathered from the internet to help the optimisation. Sources linked below.

### codon_rules.py
Set of rules that can be implemented as a group or by themselves. Each rule optimises the virus on X amount.

### codon_tools.py
Set of tools manipulate codons.

### codon_debug_tools.py
Set of tools to help to debug. 

### TerminalColor.py
Module to give colour to terminal output. 

## Rules
Explanation of the rules for optimisation.

### Disclaimer
Each rule performs an X amount of optimisation. The codon_optimisation.py script runs the rules in a particular order, 
with a particular setup. Even though all the rules perform a positive optimisation, not all the rules are implemented.

By trial and error, I found the best scenario for this particular virus, different viruses can benefit from different setups.

The proper way to check if a setup is generating a positive optimisation is using RNA Folding software.

An online version can be found here [RNAFold](http://rna.tbi.univie.ac.at//cgi-bin/RNAWebSuite/RNAfold.cgi)

I do not have the hardware to run this kind of software, each folding on my Mac was taking over an hour. I also do not have the knowledge to analyse the results. I was trying to get the scaling factor and free energy of my optimisations as close to the vaccine so, I would get a script as generic as possible. I quit this approach as I believe that the RNA optimisation
is not as simple as that, and I did not see any improvements.

The final setup is based on the highest average compatibility that I got, as the [BertHub Challenge](https://berthub.eu/articles/posts/part-2-reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/) 
uses an average of 20 runs. 

### Chances of Swap
Chances of swap is a parameter to be set between 0 and 1 (0% - 100%). This determines the percentage of time that a 
codon will be swapped in an optimisation. 

---
## Rules Included
These are the rules that I included in my final optimisation.

### Most Common
It is known as the most common codons in Human RNA. The frequency of these codons is mapped on the codon_properties.py file.

This rule goes through every codon, for each codon gets its amino acid. Then from the amino acid looks into the frequency map and gets the most common codon. Finally, replaces the virus codon to the most common found in Human 
body.

### Avoiding Patterns
According to [Wikipedia - List of restriction enzyme cutting sites: Bsa–Bso](https://en.wikipedia.org/wiki/List_of_restriction_enzyme_cutting_sites:_Bsa–Bso) 
some enzymes have a sequence to be avoided, some libraries such as [RNAlib](https://www.tbi.univie.ac.at/RNA/ViennaRNA/doc/html/examples_python.html) 
have the option to avoid these sequences.

I learnt that if I avoid this pattern not for the entire sequence but from a certain position I get some improvement.

To avoid the pattern this rule swaps the target codon to a correspondent **second most frequent** codon based on the frequency map. 


Ex.:

- Given the sequence: *TTTTTCCAGAGTTTTTT* where I find **10 CCAGAG** patterns.

I can set this rule to start avoiding the pattern from 50% of the sequence, 
or from the 5th pattern onwards.

I also learnt that if I swap a position (first, second, or third) of subsequent codons in the pattern 
I get some improvement.

Ex.:

- Given the sequence: *TTTTTCCAGAGTTTTTT*
- Look for pattern: *CCAGAG*

1. Position - Codon: **TTC**
2. Position - Codon: **CAG**
3. Position - Codon: **AGT**

Some patterns I get improvement if I swap the codon on the first position, others I get improvement if I swap the codon on the third position. *Based on trial and error.*

#### Avoid CCAGAG Pattern rule
- Avoids the enzyme **BsaI**
- Start from **70%**
- Swap **third** position

#### Avoid GCAGAG Pattern rule
- Avoids the enzyme **BsmBI**
- Start from **the beginning**
- Swap **third** position

#### Avoid CCANTGG Pattern rule
- Avoids the enzyme **Bse64I**
- Start from **50%**
- Swap **first** position

#### Avoid CCCTG Pattern rule
- Avoids the enzyme **BslFI**
- Start from **the beginning**
- Swap **first** position

#### Avoid TGATCA Pattern rule
- Avoids the enzyme **BsiQI**
- Start from **the beginning**
- Swap **third** position

#### Avoid ACTGG Pattern rule
- Avoids the enzyme **Bse1I**
- Start from **the beginning**
- Swap **first** position

#### Avoid GGTNACC Pattern rule
- Avoids the enzyme **Bse64I**
- Start from **50%**
- Swap **third** position

#### Avoid GGATG Pattern rule
- Avoids the enzyme **BseGI**
- Start from **the beginning**
- Swap **third** position

### TTC + Q to TTT rule
I found this pattern comparing the virus RNA to the vaccine RNA that when a codon *TTC* followed
by an amino acid *Q* it is likely to swap the codon *TTC* to *TTT*. 

This rule uses the *Changes of Swap* parameter. By trial and error, this rule works better when applied 
**90%** of the time. 

### DKVE rule
According to [Reverse Engineering the source code of the BioNTech/Pfizer SARS-CoV-2 Vaccine](https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/)

The **K** and **V** amino acids there are both replaced by **P** or *Proline*.

It turns out that these two changes enhance vaccine efficiency enormously.

This optimisation looks for a pattern of amino acids *D->K->V->E* and swaps to *D->P->P->E* only once.

---
## Rules Excluded
There are the rules that I did not include in my final optimisation, even though they 
perform X amount of optimisation individually when added together they do not show better 
results. 

For other viruses, they could help and that is why I kept them.

### Populate N Most Common
This rule uses the same principle and mapping as the **Most Common** rule but, instead of getting the first most common looks for the N most common where N is the position to look. Ex. :<br>
- For N equals to 2 will get the second most common;
- For N equals to 3 will get the third most common, so on. <br>

This rule uses the *Changes of Swap* parameter.

### CCC to CCT rule
Analysing the vaccine RNA there is about 10% of CCC and 90% of CCT codons.

This rule swaps the codon *CCC* to *CCT* and uses the *Changes of Swap* parameter.

### CCA to CCU rule
According to [vera van noort @noort_zuit tweet](https://twitter.com/noort_zuit/status/1348924353921081344) the CCA to CCU
swap that happened on the vaccine RNA is to avoid a hairpin that may interfere with translation.

This rule swaps the codon *CCA* to *CCU* and uses the *Changes of Swap* parameter.


## Sources
- [Codon Frequency](https://pypi.org/project/python-codon-tables/) - Python Codon Tables

## Author

* **Lucas Fonseca Martins** - *COSC110 - Task 02* - [dyncoch](https://github.com/dyncoch)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
