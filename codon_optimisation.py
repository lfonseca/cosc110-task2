#! /usr/bin/python3

# Program to optimise codons of a virus_codons in an attempt to create a vaccine_codons

import codon_rules
from copy import deepcopy
import sys
from typing import NoReturn
import csv

# Verbose Mode
VERBOSE_MODE = False

# Used to store a dictionary mapping codons to the amino acids they encode
codon_amino_acid_map = {}


def get_amino_acid(codon):
    """
      Returns a character representing the amino acid encoded by the given codon.
      Note: Includes bugfix from Lucas Martins
    """
    if len(codon_amino_acid_map) <= 0:
        with open("codon-aminoacid.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                codon_row = row["codon"]
                amino_acid = row["aminoacid"]
                codon_amino_acid_map[codon_row] = amino_acid
    return codon_amino_acid_map[codon]


def read_codons(filename):
    """
      Reads in codons from the given file, placing them in order in the returned list.
    """
    codons = []
    with open(filename) as input_file:
        #  Read in three characters at a time, since each codon is three characters
        data = input_file.read(3)
        while data.strip() != "":
            codons.append(data)
            data = input_file.read(3)
    return codons


def compare(attempt, target):
    """
    Compares the given attempt to determine the percentage of
    codons in the given attempt match those in the target

    Arguments:
    attempt -- A list of codons to be compared with the target
    target -- A list of codons to be compared to

    Returns the percentage of codons that are identical between the attempt and the target
    """
    count = 0
    for i in range(len(target)):
        if attempt[i] == target[i]:
            count += 1
    return 100 * count / len(target)


def test_optimisation_average(virus_codons: list, vaccine_codons: list, n: int = 10) -> float:
    """
    Calculate the optimisation average.
    On https://berthub.eu/articles/posts/part-2-reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/ they use
    the average of 20 runs.
    :param virus_codons: virus_codons codons to optimise
    :param vaccine_codons: vaccine_codons codons to compare
    :param n: number of times to run the optimisation
    :return: the average of optimisation
    """
    total = .0
    for i in range(n):
        optimised_codons = optimise_codons(virus_codons)
        total += compare(optimised_codons, vaccine_codons)

    return total / n


def optimise_codons(codons, verbose=False):
    """
      Optimises the given codons using the method specified in readme.md.

      Arguments:
      codons -- A list of codons to optimise

      Returns an new_optimisation list of codons
    """

    codons_optimised = deepcopy(codons)

    # Most common rule
    codons_optimised = codon_rules.optimise_codons_most_common(codons_optimised, verbose=verbose)

    # Avoid CCAGAG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'CCAGAG', verbose)

    # Avoid GCAGAG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'GCAGAG', verbose)

    # Avoid CCANTGG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'CCANTGG', verbose)

    # TTC + Q to TTT rule
    codons_optimised = codon_rules.optimise_codons_ttc(codons_optimised, chances_of_swap=.9, verbose=verbose)

    # Avoid CCCTG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'CCCTG', verbose)

    # Avoid TGATCA Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'TGATCA', verbose)

    # Avoid ACTGG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'ACTGG', verbose)

    # Avoid GGTNACC Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'GGTNACC', verbose)

    # Avoid GGATG Pattern rule
    codons_optimised = codon_rules.optimise_codons_avoid_pattern(codons_optimised, 'GGATG', verbose)

    # DKVE rule
    codons_optimised = codon_rules.optimise_codons_dkve(codons_optimised, verbose=verbose)

    return codons_optimised


def get_verbose_mode() -> bool:
    """
    Check if parameter -v has been passed via terminal to set VERBOSE_MODE ON/OFF
    :return: True for VERBOSE_MODE ON, False to OFF
    """
    if len(sys.argv) > 1:
        for arg in sys.argv:
            if str(arg).lower() == '-v':
                return True
    return False


def show_average_mode(virus_codons: list, vaccine_codons: list) -> NoReturn:
    """
    Shows the 20 runs average if parameter -a has been passed via terminal.
    :param virus_codons: list of codons from virus RNA file
    :param vaccine_codons: list of codons from vaccine RNA file
    :return: NoReturn
    """
    if len(sys.argv) > 1:
        for arg in sys.argv:
            if str(arg).lower() == '-a':
                optimisation_average = test_optimisation_average(virus_codons, vaccine_codons, 20)
                print("Optimisation improves on average compatibility by {:.2f}%".format(optimisation_average))


if __name__ == "__main__":
    # Set Verbose Mode
    VERBOSE_MODE = get_verbose_mode()

    #  Read in virus_codons
    virus = read_codons("virus.txt")

    #  Read in vaccine_codons
    vaccine = read_codons("vaccine.txt")

    #  Optimise
    optimised = optimise_codons(virus, VERBOSE_MODE)

    #  Compare virus_codons and optimised version to vaccine_codons
    virus_comparison = compare(virus, vaccine)
    optimised_comparison = compare(optimised, vaccine)

    #  Output results, including improvement from optimisation
    print("Virus comparison: {:.2f}% identical to vaccine".format(virus_comparison))
    print("Optimised comparison: {:.2f}% identical to vaccine".format(optimised_comparison))
    print("Optimisation improves compatibility by {:.2f}%".format(optimised_comparison - virus_comparison))

    # Average of optimisation
    show_average_mode(virus, vaccine)
