"""
Codon manipulation tools.

@author Lucas Fonseca Martins
@date April 2021
"""
import codon_properties
from copy import deepcopy
from codon_debug_tools import vprint, verror
from typing import NoReturn
import csv

# Global codon amino acid map - Load just once.
codon_amino_acid_map = {}


class CodonTools:
    def __init__(self):
        self.codon_amino_acid_map = codon_amino_acid_map  # Reference copy
        self.codons_frequencies_sorted = {}
        self.codons_frequencies = codon_properties.codons_frequencies
        self.codons_pattern_setup = codon_properties.codons_pattern_setup

        # Map amino acid
        self.map_amino_acid()

    def map_amino_acid(self) -> NoReturn:
        """
        Maps the codons with the respective amino acid based on the file codon-aminoacid.csv
        It will only map once, if codon_amino_acid_map has been set before it won't update.
        :return: NoReturn
        """
        if not self.codon_amino_acid_map:
            with open("codon-aminoacid.csv") as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    codon_row = row["codon"]
                    amino_acid = row["aminoacid"]
                    self.codon_amino_acid_map[codon_row] = amino_acid

    def get_amino_acid(self, codon: str) -> str:
        """
        Returns a character representing the amino acid encoded by the given codon.
        :param codon: codon sequence
        :return: amino acid
        """
        return self.codon_amino_acid_map[codon]

    def sort_codon_properties(self) -> NoReturn:
        """
        Sorts the codon_frequencies map using the keys as reference and creates a new map.
        :return: NoReturn
        """
        codons_frequencies_cp = deepcopy(self.codons_frequencies)  # a shallow copy won't work
        for key in self.codons_frequencies.keys():
            self.codons_frequencies_sorted[key] = dict(sorted(codons_frequencies_cp[key].items(),
                                                              key=lambda item: item[1]))

    def get_most_common(self, codon: str) -> str:
        """
        Given a codon looks for its amino acid and based on that returns
        the most common codon based on the codons_frequencies dictionary.

        :param codon: codon to be swapped
        :return: most common codon based on amino acid
        """
        amino_acid = self.get_amino_acid(codon)
        codon_average = self.codons_frequencies[amino_acid]
        return max(codon_average, key=lambda key: codon_average[key])

    def get_n_most_common(self, codon: str, n: int = 2) -> str:
        """
        Given a codon looks for its amino acid and based on that returns returns the
        N most common codon based on the codons_frequencies dictionary.
        If there is none, return the most common instead.

        :param n: get the n most common, eg.: second most common n = 2, third most common n = 3
        :param codon: codon to be swapped
        :return: N most common codon, if none returns most common
        """
        try:
            n = int(n)
        except ValueError:
            verror(f"Value of n has to be an integer: {ValueError}", True)
            return codon

        amino_acid = self.get_amino_acid(codon)
        if not self.codons_frequencies_sorted:
            self.sort_codon_properties()

        if len(self.codons_frequencies_sorted[amino_acid]) >= n:
            return list(self.codons_frequencies_sorted[amino_acid])[-n]
        else:
            return codon

    @staticmethod
    def get_codons_as_string(codons: list) -> str:
        """
        Concatenates codons list into a string
        :param codons: codons list
        :return: codons string
        """
        return ''.join(codons)
