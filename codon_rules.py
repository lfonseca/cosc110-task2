"""
Codons rules for optimisation.
Each rule can be called separately.

@author Lucas Fonseca Martins
@date April/2021
"""
from codon_tools import CodonTools
from codon_debug_tools import vprint, verror
import re
from random import random
from copy import deepcopy


def optimise_codons_most_common(codons: list, verbose: bool = False) -> list:
    """
    Gets the most common codons from a map and creates a new list.
    :param codons: lists of codons to be optimised
    :param verbose: if True print logs on console
    :return: list of codons optimised
    """
    i = 0
    cnt = 0
    ct = CodonTools()
    vprint("Initializing Most Common optimisation...", verbose)
    new_optimisation = []
    for codon in codons:
        new_codon = ct.get_most_common(codon)
        new_optimisation.append(new_codon)
        if codon != new_codon:
            vprint(f"Swap position {i}: Codon {codon} | New Codon {new_codon}", verbose)
            cnt += 1
        i += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint("End of Most Common optimisation.\n", verbose)
    return new_optimisation


def optimise_populate_n_most_common(codons, n: int = 2, verbose: bool = False, chances_of_swap: float = 0.01) -> list:
    """
    Gets the N most common codons from a map and creates a new list.
    Where N is the position, eg.: N = 2 -> Second most common; N = 3 -> Third most common.
    :param codons: lists of codons to be optimised
    :param n: the position
    :param verbose: if True print logs on console
    :param chances_of_swap: percentage of chance to swap: 0 to 1
    :return: list of codons optimised
    """
    cnt = 0
    i = 0
    ct = CodonTools()
    new_optimisation = []
    vprint(f"Initializing Popularization {n} Most Common optimisation...", verbose)
    for codon in codons:
        n_most_common_codon = ct.get_n_most_common(codon, n=n)
        if codon != n_most_common_codon and random() < chances_of_swap:
            vprint(f"Swap position {i}: Codon {codon} | New Codon {n_most_common_codon}", verbose)
            new_optimisation.append(n_most_common_codon)
            cnt += 1
        else:
            new_optimisation.append(codon)
        i += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint(f"End of Popularization {n} Most Common optimisation.\n", verbose)
    return new_optimisation


def optimise_codons_ttc(codons: list, chances_of_swap=1.0, verbose: bool = False) -> list:
    """
    I found this pattern that it is very likely that a codon TTC followed by a amino acid Q can be improved by
    swapping TTC to TTT.
    :param codons: list of codons to be optimised
    :param chances_of_swap: percentage of chance to swap: 0 to 1
    :param verbose: if True print logs on console
    :return: list of codons optimised
    """
    cnt = 0
    ct = CodonTools()
    vprint("Initializing TTC + Q to TTT optimisation", verbose)
    new_optimisation = deepcopy(codons)
    for i in range(len(new_optimisation)):
        if new_optimisation[i] == 'TTC' and ct.get_amino_acid(
                new_optimisation[i + 1]) == 'Q' and random() < chances_of_swap:
            vprint(f"Swap position {i}", verbose)
            new_optimisation[i] = 'TTT'
            cnt += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint("End of TTC + Q to TTT optimisation.\n", verbose)
    return new_optimisation


def optimise_codons_cca(codons: list, codons_original: list = None, chances_of_swap=1.0, verbose: bool = False) -> list:
    """
    According to: https://twitter.com/noort_zuit/status/1348924353921081344
    This change could prevent a “hairpin” in the RNA.
    :param codons: lists of codons to be optimised
    :param codons_original: list of original codons NEVER optimised
    :param chances_of_swap: percentage of chance to swap: 0 to 1
    :param verbose: if True print logs on console
    :return: list of codons optimised
    """
    cnt = 0
    vprint("Initializing CCA optimisation", verbose)
    new_optimisation = deepcopy(codons)
    codons_original = new_optimisation if not codons_original else codons_original
    for i in range(len(new_optimisation)):
        if codons_original[i] == 'CCA' and random() < chances_of_swap:
            vprint(f"Swap position {i}", verbose)
            new_optimisation[i] = 'CCU'
            cnt += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint("End of CCA optimisation.\n", verbose)
    return new_optimisation


def optimise_codons_dkve(codons: list, verbose: bool = False) -> list:
    """
    According to: https://berthub.eu/articles/posts/reverse-engineering-source-code-of-the-biontech-pfizer-vaccine/
    The K and V amino acids there are both replaced by ‘P’ or Proline.
    It turns out that these two changes enhance the vaccine_codons efficiency enormously.
    This optimisation looks for a pattern of amino acids D->K->V->E and swaps to D->P->P->E
    :param codons: lists of codons to be optimised
    :param verbose: if True print logs on console
    :return: list of codons optimised
    """
    cnt = 0
    ct = CodonTools()
    vprint("Initializing DKVE optimisation", verbose)
    new_optimisation = deepcopy(codons)
    for i in range(len(new_optimisation)):
        if i + 3 < len(new_optimisation):
            if ct.get_amino_acid(new_optimisation[i]) == 'D' \
                    and ct.get_amino_acid(new_optimisation[i + 1]) == 'K' \
                    and ct.get_amino_acid(new_optimisation[i + 2]) == 'V' \
                    and ct.get_amino_acid(new_optimisation[i + 3]) == 'E':
                new_optimisation[i + 1] = 'CCT'
                new_optimisation[i + 2] = 'CCT'
                vprint(f"Swap positions {i + 1} and {i + 2}", verbose)
                cnt += 2
                break  # We should to this only once
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint("End of DKVE optimisation.\n", verbose)
    return new_optimisation


def optimise_codons_cct(codons: list, chances_of_swap=1.0, verbose: bool = False) -> list:
    """
    The vaccine_codons has way more CCT codons than CCC, but not 100% CCT.
    I learnt that if I swap 90% of the time I can improve the optimisation by up to 0.5%

    :param codons: list of codons to be optimised
    :param chances_of_swap: percentage of chance to swap: 0 to 1
    :param verbose: if True print logs on console
    :return: list of codons optimised
    """
    cnt = 0
    vprint("Initializing CCT optimisation", verbose)
    new_optimisation = deepcopy(codons)
    for i in range(len(new_optimisation)):
        if new_optimisation[i] == 'CCC' and random() < chances_of_swap:
            new_optimisation[i] = 'CCT'
            vprint(f"Swap position {i}", verbose)
            cnt += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint("End of CCT optimisation.\n", verbose)
    return new_optimisation


def __codon_pattern_search__(codons: list, pattern: str) -> list:
    """
    Searches for a pattern on codons list but, this pattern can be spread onto multiple codons.
    So this function concatenates the codons list into a string, searches for a pattern.
    From the start position of a pattern looks for what would be the position of the begging of a codon.
    Finally, returns a list with the codons' positions that contain the pattern.
    :param pattern: Pattern to be searched for
    :param codons: List of codons to be looked for a pattern
    :return: A list of positions on codons list that matches to the begging of a pattern
    """
    positions = []
    pattern = pattern.replace('N', '?')  # some patterns can have a N which is any character one time
    codons_str = CodonTools.get_codons_as_string(codons)
    locations = [m.start() for m in re.finditer(pattern, codons_str)]
    for loc in locations:
        reminder = loc % 3  # because codons are blocks of 3 letters we have to split the string again
        new_loc = loc - reminder
        pos = int(new_loc / 3)
        positions.append(pos)
    return positions


def __avoid_pattern_x__(codons: list, limit: float = .5, swap_position: str = "third",
                        verbose: bool = False, pattern: str = 'X') -> list:
    """
    Avoids a pattern in a list of codons.

    Swap Position
    -------------
    Given the sequence:
    TTTTTCCAGAGTTTTTT

    I have to locate the codon where the patter starts:
    codon[x-1] = TTT
    codon[x] = TTC -> first position
    codon[x+1] = CAG -> second position
    codon[x+2] = AGT -> third position
    codon[x+3] = TTT

    Limit
    -----
    Given the sequence where I found 10 CCAGAG patterns:
    limit = .5 will start avoiding the pattern from 50% of the sequence, or from the 5th pattern onwards.

    :param codons: list of codons to be optimised
    :param limit: percentage of patterns to be avoided
    :param swap_position: codon position to be swapped on a pattern
    :param verbose: if True print logs on console
    :param pattern: pattern to be looked for
    :return: list of codons optimised
    """
    cnt = 0
    codon_to_swap = {
        "first": 0,
        "second": 1,
        "third": 2
    }
    vprint(f"Initializing Avoid {pattern} Pattern optimisation", verbose)
    new_optimisation = deepcopy(codons)
    positions = __codon_pattern_search__(new_optimisation, pattern)
    ct = CodonTools()
    current_position = 0

    # Get start position based on limit
    if not (0 <= limit <= 1):
        raise Exception('limit has to be between 0 and 1')
    start_from_position = int(len(positions) * limit)
    vprint(f"Starting from position {start_from_position}", verbose)

    for pos in positions:
        if current_position >= start_from_position:
            new_codon = ct.get_n_most_common(new_optimisation[pos + codon_to_swap[swap_position]], 2)
            vprint(
                f'Swap positions: {pos} Codon: {new_optimisation[pos + codon_to_swap[swap_position]]} | New Codon: {new_codon} '
                , verbose)
            new_optimisation[pos + codon_to_swap[swap_position]] = new_codon
            cnt += 1
        current_position += 1
    vprint(f"Codons swapped: {cnt}", verbose)
    vprint(f"End of Avoid {pattern} Pattern optimisation.\n", verbose)
    return new_optimisation


def optimise_codons_avoid_pattern(codons: list, pattern: str, verbose: bool = False) -> list:
    """
    According to Wikipedia some enzymes have a sequence to be avoided, some algorithms have the option to avoid this
    sequence.
    I learnt that if I avoid this pattern not for the entire sequence but from a certain position I get some improvement
    I also learnt that if I swap the third subsequents codons in the pattern I get some improvement.
    Source: https://en.wikipedia.org/wiki/List_of_restriction_enzyme_cutting_sites:_Bsa–Bso
    :param codons: list of codons to be optimised
    :param pattern: pattern to be avoid
    :param verbose: if True print logs on console
    :return: codons optimised
    """
    ct = CodonTools()
    try:
        limit = ct.codons_pattern_setup[pattern]['limit']
        swap_position = ct.codons_pattern_setup[pattern]['swap_position']
    except KeyError:
        verror(f'Invalid Pattern: {pattern}', verbose)
        return codons
    else:
        return __avoid_pattern_x__(codons, limit, swap_position, verbose, pattern)
