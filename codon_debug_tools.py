"""
Tools to print warnings, errors and create logs.
Not related to codon_optimisation, just for debugging.

@author Lucas Fonseca Martins
@date April/2021
"""

from TerminalColor import create_warning_msg, create_error_msg
from datetime import datetime
from os import path, makedirs
from typing import NoReturn

LOGS_DIR = 'logs'
log_file_name = ""


def vprint(text: str, verbose: bool):
    """
    Function to verbose, only print if VERBOSE_MODE is True
    :param text: Message to be printed
    :param verbose: If True prints the message
    :return: None
    """
    if verbose:
        print(create_warning_msg(text))
        # Log
        append_to_log(text)


def verror(text: str, verbose: bool):
    """
    Function to verbose error, only print if VERBOSE_MODE is True
    :param text: Error message to be printed
    :param verbose: If True prints the message
    :return: None
    """
    if verbose:
        print(create_error_msg(text))
        # Log
        append_to_log(text)


def create_log_name() -> NoReturn:
    """
    Creates a log path name based on datetime and log directory and sets to a global variable log_file_name
    :return: NoReturn
    """
    global log_file_name
    log_file_name = 'log_' + datetime.today().strftime('%Y%m%d%H%M%S') + '.txt'
    log_file_name = path.join(LOGS_DIR, log_file_name)


def create_log_file() -> NoReturn:
    """
    Creates a log file based on path log name global variable and writes the first line with Date and Time
    :return: NoReturn
    """
    with open(log_file_name, "x") as log:
        log.write("Codon Optimisation Log - " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))


def check_log_file() -> NoReturn:
    """
    Checks if log file path name exists if not creates one;
    Checks if log directory exists if not creates one;
    Checks if log file exists if not creates one.
    :return: NoReturn
    """
    if not log_file_name:
        create_log_name()
        print(f'Creating log file: {log_file_name}')

    if not path.isdir(LOGS_DIR):
        makedirs(LOGS_DIR)

    if not path.isfile(log_file_name):
        create_log_file()


def append_to_log(text: str) -> NoReturn:
    """
    Append a text to the end of a log file. If the log file doesn't exists, creates one.
    :param text: Text to be appended to the end of log file
    :return: NoReturn
    """
    check_log_file()
    with open(log_file_name, "a") as log:
        log.write(f'\n{text}')
